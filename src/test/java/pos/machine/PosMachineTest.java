package pos.machine;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class PosMachineTest {

    @Test
    public void should_return_receipt() {
        PosMachine posMachine = new PosMachine();

        String expected = "***<store earning no money>Receipt***\n" +
                "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n" +
                "**********************";

        assertEquals(expected, posMachine.printReceipt(loadBarcodes()));
    }
    @Test
    public void should_return_barcode_quantity_map() {
        PosMachine posMachine = new PosMachine();
        Map<String, Long> mapBarcodeToQuantity = posMachine.mapBarcodeToQuantity(loadBarcodes());
        assertEquals(4, mapBarcodeToQuantity.get("ITEM000000"));
        assertEquals(3, mapBarcodeToQuantity.get("ITEM000004"));
        assertEquals(2, mapBarcodeToQuantity.get("ITEM000001"));
    }

    @Test
    public void should_return_items_by_barcodes() {
        PosMachine posMachine = new PosMachine();
        List<String> barcodes = loadBarcodes();
        List<Item> items = posMachine.getItemsByBarcodes(barcodes);
        Set<String> expectedBarcodesSet = items.stream().map(item -> item.getBarcode()).collect(Collectors.toSet());
        List<String> distinctBarcodes = barcodes.stream().distinct().collect(Collectors.toList());
        assertTrue(expectedBarcodesSet.contains(distinctBarcodes.get(0)));
        assertTrue(expectedBarcodesSet.contains(distinctBarcodes.get(1)));
        assertTrue(expectedBarcodesSet.contains(distinctBarcodes.get(2)));
        assertFalse(expectedBarcodesSet.contains("ITEM00009"));
    }

    @Test
    public void should_return_correct_subtotal() {
        PosMachine posMachine = new PosMachine();
        Integer quantity = 10;
        Integer price = 20;
        Integer expectedSubtotal = 200;
        assertEquals(expectedSubtotal, posMachine.calculateSubtotal(quantity, price));
    }

    @Test
    public void should_return_correct_total() {
        PosMachine posMachine = new PosMachine();
        List<Integer> subtotals = Arrays.asList(10,20,40,50);
        Integer expectedSubtotal = 120;
        assertEquals(expectedSubtotal, posMachine.calculateTotal(subtotals));
    }

    @Test
    public void should_return_itemsOutput() {
        PosMachine posMachine = new PosMachine();
        String expected = "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n";
        assertEquals(expected, posMachine.buildItemsOutput(ItemsLoader.loadAllItems(),
                posMachine.mapBarcodeToQuantity(loadBarcodes())));
    }

    @Test
    public void should_return_receipt_output() {
        PosMachine posMachine = new PosMachine();
        String expected = "***<store earning no money>Receipt***\n" +
                "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n" +
                "**********************";
        String itemsOutput = posMachine.buildItemsOutput(ItemsLoader.loadAllItems(),
                posMachine.mapBarcodeToQuantity(loadBarcodes()));
        assertEquals(expected, posMachine.buildReceiptOutput(itemsOutput));
    }

    private static List<String> loadBarcodes() {
        return Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004");
    }
}
